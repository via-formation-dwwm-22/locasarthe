<?php

require_once './db.php';
// Comme pour l'affichage du client, je viens chercher le client correspondant à l'id en get
// cel me permettera de préremplire les champs du formulaire
if (!empty($_GET["id"])) {
    $id = (int)$_GET["id"];
    $request = $pdo->prepare("SELECT * FROM client WHERE id=:identifier");
    $request->execute(["identifier" => $id]);
    $client = $request->fetch(PDO::FETCH_ASSOC);


    // je redirige l'utilisateur sur l'accueuil si l'id est vide ou invalide
    if (!$client) {
        header('Location:/index.php');
    }
} else {
    header('Location:/index.php');
}


$message = "";
// je vérifie que le formulaire a été envoyé
// je verifie que les differents champs ont bien étés envoyés
if (isset($_POST['submit']) && !empty($_POST['nom']) && !empty($_POST['prenom']) && !empty($_POST['adresse']) && !empty($_POST['cp']) && !empty($_POST['ville']) && !empty($_POST['permis']) && !empty($_POST['bdate']) && !empty($_POST['pdate']) && isset($_POST['tel'])) {
    // je vérifie la taille du num de permis
    if (strlen($_POST["permis"]) <= 12) {
        // je vérifie que le numéro n'est pas déja pris (par un autre client)
        $request_permis = $pdo->prepare("SELECT id, nom, prenom FROM client WHERE numero_permis = :num_permis");
        $request_permis->execute(["num_permis" => $_POST["permis"]]);
        $client2 = $request_permis->fetch(PDO::FETCH_ASSOC);
        if (!$client2 or $client2['id'] === $client["id"]) {
            // je met mon client à jour (je met tout les champs à jour d'un coup)
            $updateRequest = $pdo->prepare("UPDATE client SET nom = :nom, prenom = :prenom, adresse = :adresse, code_postal = :code_postal, ville = :ville, telephone = :telephone, email = :email, date_naissance = :date_naissance, numero_permis = :numero_permis, date_permis = :date_permis WHERE id = :id");
            $updateRequest->execute([
                "id" => $client["id"],
                "nom" => $_POST["nom"],
                "prenom" => $_POST["prenom"],
                "adresse" => $_POST["adresse"],
                "code_postal" => $_POST["cp"],
                "ville" => $_POST["ville"],
                "telephone" => $_POST["tel"],
                "email" => $_POST["email"],
                "date_naissance" => $_POST["bdate"],
                "numero_permis" => $_POST["permis"],
                "date_permis" => $_POST["pdate"],
            ]);

            // je recharge la page (je recupère l'url actuelle, puis je fais une redirection sur cette url)
            $url = $_SERVER["REQUEST_URI"];
            header("Location: ".$url);
            // je créé un cookie pour sauvegarder le message de réussite
            setcookie("message", "Le client a bien été modifié");
        } else {
            $message = 'Ce numero de permis est déjà enregistré pour le client '. $client2['prenom'] . ' '.$client2['nom'];
        }
    }else{
        $message = "Numéro de permis non conforme!";
    }
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Modification</title>
</head>
<body>

<h1>Modifier un client</h1>
<p><?php
    // pour lire le cookie, j'utilise la variable $_COOKIE
    if(isset($_COOKIE["message"])){
        echo $_COOKIE["message"];
        // je modifie la tate d'expiration du cookie pour le rendre périmé
        // et qu'il disparaisse
        setcookie("message", "", time()-1);
    }
    ?></p>
<form method="POST">
    <!-- je préremplie les champs avec les valeures de la bdd -->
    <input type="text" name="nom" placeholder="Nom" value="<?= $client['nom'] ?>" required>
    <input type="text" name="prenom" value="<?= $client['prenom'] ?>" placeholder="Prénom" required>
    <input type="text" name="adresse" value="<?= $client['adresse'] ?>" placeholder="Adresse" required>
    <input type="text" name="cp" value="<?= $client['code_postal'] ?>" placeholder="Code Postal" required>
    <input type="text" name="ville" value="<?= $client['ville'] ?>" placeholder="Ville" required>
    <input type="tel" name="tel" value="<?= $client['telephone'] ?>" placeholder="Télephone">
    <input type="email" name="email" value="<?= $client['email'] ?>" placeholder="Email">
    <br>
    <label> Date de naissance
        <input type="date" name="bdate" value="<?= $client['date_naissance'] ?>" required>
    </label>
    <br>
    <input type="text" name="permis" value="<?= $client['numero_permis'] ?>" placeholder="Numéro de permis" required>
    <label> Date d'octroi de permis
        <input type="date" name="pdate" value="<?= $client['date_permis'] ?>" required>
    </label>
    <input type="submit" name="submit" value="Sauvegarder">
</form>
</body>
</html>

