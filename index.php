<?php 
require_once './db.php';

$sql = $pdo->prepare('SELECT id, nom, prenom, ville, telephone, email FROM client ORDER BY nom ASC;');
$sql->execute();
$clients = $sql->fetchAll(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Locasarthe - Accueil</title>
    <link rel="stylesheet" href="/style.css" type="text/css">
</head>
<body>
    <header>
        <nav>
            <ul>
                <li>
                    <a href="/">Accueil</a>
                </li>
            </ul>
        </nav>
    </header>
    <main>
        <h1>Bienvenue sur Locasarthe</h1>
        <section>
            <h2>Liste des clients</h2>
            <a href="/create_client.php">Ajouter un client</a>
            <div id="clients" class="table">
                <div class="table_head">
                    <div>Nom :</div>
                    <div>Ville :</div>
                    <div>Tél :</div>
                    <div>Email :</div>
                    <div></div>
                </div>

                <?php foreach ($clients as $client): ?>
                    <div class="table_content">
                        <div>
                            <a href="/client.php?id=<?= $client['id']; ?>">
                                <?= $client['prenom'] . ' ' . $client['nom']; ?>
                            </a>
                        </div>
                        <div><?= $client['ville']; ?></div>
                        <div><?= $client['telephone']; ?></div>
                        <div><?= $client['email']; ?></div>
                        <div>
                            <a href="update_client.php?id=<?= $client['id']; ?>">Modifier</a>
                            <a href="delete_client.php?id=<?= $client['id']; ?>">supprimer</a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </section>
    </main>
</body>
</html>
