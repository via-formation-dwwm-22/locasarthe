<?php
    require_once './db.php';    
    if (!empty($_GET['id'])){
        $id = (int) $_GET['id'];
        $sql = $pdo->prepare('SELECT * FROM client WHERE id=:id_client');
        $sql->execute([
            'id_client' => $id
        ]);
        $client = $sql->fetch(PDO::FETCH_ASSOC);

        if (!$client){
            header('Location:/index.php');
        }
    }
    else {
        header('Location:/index.php');
    }

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Locasarthe - Détail client</title>
    <link rel="stylesheet" href="/style.css" type="text/css">
</head>

<body>
    <header>
        <nav>
            <ul>
                <li>
                    <a href="/">Accueil</a>
                </li>
            </ul>
        </nav>
    </header>
    <main>
        <h1>Détails des informations de <?= $client['prenom'] ?> <?=  $client['nom'] ?></h1>
        <div>
            <strong>Nom : </strong> <?=  $client['nom'] ?>
        </div>
        <div>
            <strong>Prénom : </strong> <?=  $client['prenom'] ?>
        </div>
        <div>
            <strong>Adresse : </strong> <?=  $client['adresse'] ?> <?=  $client['code_postal'] ?> <?=  $client['ville'] ?>
         </div>
        <div>
            <strong>Téléphone : </strong> <?=  $client['telephone'] ?>
        </div>
        <div>
            <strong>Email : </strong> <?=  $client['email'] ?>
        </div>
        <div>
            <strong>Date de Naissance : </strong> <?=  date('d/m/Y', strtotime($client['date_naissance'])); ?>
        </div>
        <div>
            <strong>Permis : </strong> n° <?=  $client['numero_permis'] ?> du <?=  date('d/m/Y', strtotime($client['date_permis'])); ?>
        </div>

        <a href="/delete_client.php?id=<?= $client['id']; ?>">supprimer</a>
    </main>
</body>

</html>
