<?php 
require_once './db.php';

if (!empty($_GET['id'])){
    $id = (int) $_GET['id'];
    $sql = $pdo->prepare('SELECT * FROM client WHERE id=:id_client');
    $sql->execute([
        'id_client' => $id
    ]);
    $client = $sql->fetch(PDO::FETCH_ASSOC);

    if (!$client){
        header('Location:/index.php');
    }
    else {
        $sql = $pdo->prepare('SELECT * FROM location WHERE id_client=:id_client;');
        $sql->execute([
            'id_client' => $id
        ]);
        $locations = $sql->fetchAll();
    }
} elseif(isset($_POST['submit']) && !empty($_POST['id'])){
    $id = (int) $_POST['id'];
    $sql_loc = $pdo->prepare('DELETE FROM location WHERE id_client=:id_client;');
    $sql_loc->execute([
        'id_client' => $id
    ]);

    $sql = $pdo->prepare('DELETE FROM client WHERE id=:id_client;');
    $sql->execute([
        'id_client' => $id
    ]);
    header('Location:/index.php');
}
else {
    header('Location:/index.php');
}
 ?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Locasarthe - Suppression</title><link rel="stylesheet" href="/style.css" type="text/css">
</head>
<body>
    <header>
        <nav>
            <ul>
                <li>
                    <a href="/">Accueil</a>
                </li>
            </ul>
        </nav>
    </header>
    <main>
        <h1>Êtes-vous sûr de vouloir supprimer ce client ?</h1>
        <div>
            <?= $client['prenom'].' '.$client['nom']; ?>
        </div>
        <form action="/delete_client.php" method="POST">
            <input type="hidden" name="id" value="<?= $client['id']?>">
            <input type="submit" name="submit" value="Oui">
            <a href="/index.php">Non</a>
        </form>
        <?php if (!empty($locations)): ?>
            <h2>Attention, vous allez supprimer les locations suivantes:</h2>
            <div id="locations" class="table">
                <div class="table_head">
                    <div>Numéro de résa :</div>
                    <div>Date de réservation :</div>
                    <div>Date de départ :</div>
                    <div>Date de retour :</div>
                    <div></div>
                </div>

                <?php foreach ($locations as $location): ?>
                    <div class="table_content">
                        <div>
                            <?= $location['numero_resa'] ?>
                        </div>
                        <div><?= $location['date_resa']; ?></div>
                        <div><?= $location['date_depart']; ?></div>
                        <div><?= $location['date_retour']; ?></div>
                        <div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        
        <?php else:?>
            <h2>Ce client n'a pas de réservation</h2>
        <?php endif; ?>
    </main>
</body>
</html>