<?php 
require_once './db.php';
$message = "";

if(isset($_POST['submit']) && !empty($_POST['nom']) && !empty($_POST['prenom']) && !empty($_POST['adresse']) && !empty($_POST['cp']) && !empty($_POST['ville']) && !empty($_POST['permis']) && !empty($_POST['bdate']) && !empty($_POST['pdate'])){
    $num_permis = htmlspecialchars($_POST['permis']);
    if (strlen($num_permis) <= 12){
        $sql = $pdo->prepare("SELECT nom, prenom FROM client WHERE numero_permis=:permis");
        $sql->execute([
            'permis'    => $num_permis
        ]);
        $client = $sql->fetch();

        if(!$client){
            $sql = $pdo->prepare("INSERT INTO client VALUE (null, :nom, :prenom, :adresse, :cp, :ville, :tel, :email, :bdate, :permis, :pdate)");
            /* Attention à ne pas mettre duirectement les $_POST mais à sanitizer les saisies utilisateurs avant */
            $sql->execute([
                'nom' => $_POST['nom'],
                'prenom'    => $_POST['prenom'], 
                'adresse'   => $_POST['adresse'],
                'cp'    => $_POST['cp'],
                'ville' => $_POST['ville'],
                'tel' => $_POST['tel'], 
                'email' => $_POST['email'], 
                'bdate' => $_POST['bdate'],
                'permis' => $num_permis,
                'pdate' => $_POST['pdate']
            ]);
            header('Location:/index.php');
        }
        else{
            $message = 'Ce numero de permis est déjà enregistré pour le client '. $client['prenom'] . ' '.$client['nom'];
        }
    }
    else{
        $message = "Numéro de permis non conforme!";
    }
}
else{
    $message = "C'est pas bien rempli!!!";
}

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Locasarthe - Nouveau client</title>
    <link rel="stylesheet" href="/style.css" type="text/css">
</head>
<body>
    <header>
        <nav>
            <ul>
                <li>
                    <a href="/">Accueil</a>
                </li>
            </ul>
        </nav>
    </header>
    <main>
        <h1>Ajouter un client</h1>
        
        <form method="POST">
            <input type="text" name="nom" placeholder="Nom" required>
            <input type="text" name="prenom" placeholder="Prénom" required>
            <input type="text" name="adresse" placeholder="Adresse" required>
            <input type="text" name="cp" placeholder="Code Postal" required>
            <input type="text" name="ville" placeholder="Ville" required>
            <input type="tel" name="tel" placeholder="Télephone">
            <input type="email" name="email" placeholder="Email">
            <br>
            <label> Date de naissance
            <input type="date" name="bdate" required>
            </label>
            <br>
            <input type="text" name="permis" placeholder="Numéro de permis" required>
            <label> Date d'octroi de permis
            <input type="date" name="pdate" required>
            </label>
            <input type="submit" name="submit" value="Sauvegarder">
        </form>
        <div><?= $message; ?></div>
    </main>
</body>
</html>